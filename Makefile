.PHONY: voting-svc dependency

voting-svc:
	$(MAKE) -C emojivoto-voting-svc

dependency:
	go mod download
	go get -v -d ./...
